#include <iostream>
#include <fstream>
#include <math.h>
// root header files
#include </home/rogers/root/include/TFile.h>
#include </home/rogers/root/include/TTree.h>
#include </home/rogers/root/include/TTreeReader.h>
#include </home/rogers/root/include/TTreeIndex.h>
#include </home/rogers/root/include/TTreeReaderValue.h>
#include </home/rogers/root/include/TH1.h>
#include "unpackDigites.h"

#define EVENT_DURATION 500000 // 500 nsec

// To unpack waveforms the next two lines needed to be modified accordingly.  
#define NSAMP 56
#define RECORD_WAVEFORMS 0 // 1 to record waveforms, 0 for no waveforms
#define MAX_NCH 64
#define MAX_NBRD 3

using namespace std;

uint64_t starttime, event_duration = (uint64_t)50; // ns
uint64_t timestamp[64];
uint16_t finetimestamp[64];
int16_t energy[64];
uint16_t mult, wf[NSAMP], waveform[64][NSAMP];
int16_t channel[64];
float psd[64];

int main(int argc, char** argv)
{

    if (argc < 3) {
    // Tell the user how to run the program
      std::cerr << "Usage: " << argv[0] << " inputfile, outputfile, configfile (optional, default = det_config.txt)" << std::endl;
      return 0;
    }  

    char* inputfile = argv[1];
    char* outputfile = argv[2];
    TFile* ofile = new TFile(outputfile, "recreate");

    // Create the root tree and set up the branch structure
    TTree* t = new TTree("t", "Detector array tree");
    t->Branch("time[64]", &timestamp, "timestamp[64]/l");
    t->Branch("finetime[64]", &finetimestamp, "finetimestamp[64]/s");
    t->Branch("energy[64]", &energy, "energy[64]/S");
    t->Branch("psd[64]", &psd, "psd[64]/F");
    t->Branch("multiplicity", &mult, "mult/s");
    t->Branch("channel[64]", &channel, "channel[64]/S");
	//    if(RECORD_WAVEFORMS) {
	//	    char buf[100];
	//    	    sprintf(buf,"waveform[%i]/s",NSAMP);
	//	    t->Branch("waveform", &waveform, buf);}

//    ifstream config_file;
//    if(argv[3]){
//      config_file.open(argv[3]);
//    }else{
//      config_file.open("det_config.txt");
//    }
//    if(config_file.is_open()) {
//	cout<<"Success opening DET configuration file"<<endl;
//    } else {
//	cout<<"Error opening DET configuration file"<<endl;
//    }
//    config_file >> tdoffset[0] >> tdoffset[1] >> tdoffset[2] >> tdoffset[3] >> tdoffset[4] >> tdoffset[5] >> tdoffset[6] >> tdoffset[7] >> tdoffset[8] >> tdoffset[9] >> tdoffset[10] >> tdoffset[11] >> tdoffset[12] >> tdoffset[13] >> tdoffset[14] >> tdoffset[15];
//    config_file >> posslope[1] >> posslope[2] >> posslope[3] >> posslope[4] >> posslope[5] >> posslope[6] >> posslope[7] >> posslope[8] >> posslope[9] >> posslope[10] >> posslope[11] >> posslope[12] >> posslope[13] >> posslope[14] >> posslope[15];    
//    cout<<"tdiff offsets: "<<tdoffset[1]<<" "<<tdoffset[2]<<" "<<tdoffset[3]<<"..."<<endl;
//    cout<<"position slopes:: "<<posslope[1]<<" "<<posslope[2]<<" "<<posslope[3]<<"...\n"<<endl;
//    config_file.close();

    // Unpack the digites binary file
    DEVT_BANK devt_event;
    DET_BANK det_event;
    std::vector<DET_BANK> events_vec;
    FILE *InputDataFile = NULL;
    //    char InputDataFileName[500] = "";			// Input data file (off-line run) 

    int run = 1;
    
    // From digites DataFiles.cpp, to read header information from the binary file
    //
    // Open binary file and read header information, write to file

    //	char fname[200];
    char DataFormat;
    char *cfgimg;
    uint32_t header[128];
    uint32_t nbcfg;
    FILE *cfg;
//	uint32_t Nsamples;
//	uint32_t DigitizerModel, DppType, NumBrd, NumPhyCh, Tsampl, Nbit;
//	int16_t WaveformEnabled;

	InputDataFile = fopen(inputfile, "rb");
	if (InputDataFile == NULL) {
//		msg_printf(MsgLog, "ERROR: Can't open Input Data File %s\n", fname);
		return -1;
	}
cout << "Input file opened" << endl;
	// read the header of the data file
	if (fread(&DataFormat, sizeof(char), 1, InputDataFile) < 1) { // Data format (version)
//		msg_printf(MsgLog, "ERROR: Invalid format for a raw data file\n");
		return -1;
	}
	cout << "DataFormat = " << DataFormat << endl;
	if (DataFormat == 0) {
		fread(header, sizeof(uint32_t), 1, InputDataFile); // header[0] = number of 32-bit words in header
		fread(header+1, sizeof(uint32_t), header[0]-1, InputDataFile); // read the rest of the header based on header[0]-1 remaining 32-bit words
//		Nsamples       = header[1]; 
//		DigitizerModel = header[2];
//		DppType        = header[3];
//		NumBrd         = header[4];
//		NumPhyCh       = header[5];
//		Tsampl         = header[6];
//		Nbit           = header[7];
	}
//	if (Nsamples == 0) { WaveformEnabled = 0;} else { WaveformEnabled = 1;}
//	if(RECORD_WAVEFORMS) {cfgimg = (char *)malloc(1024*1024);}
	cfgimg = (char *)malloc(1024*1024);
  	char* log = ".log";
	char cfg_file[strlen(outputfile)+strlen(log)+1];
	snprintf(cfg_file, sizeof( cfg_file ), "%s%s", outputfile, log );
	cfg = fopen(cfg_file, "w");
	if (cfg != NULL) {
		fread(&nbcfg, sizeof(uint32_t), 1, InputDataFile);
		if (fread(cfgimg, sizeof(char), nbcfg, InputDataFile) < nbcfg) {
			free(cfgimg);
//			msg_printf(MsgLog, "ERROR: Invalid data in input file\n");
			return -1;
		}
		fwrite(cfgimg, sizeof(char), nbcfg, cfg);
	}
	fclose(cfg);
	free(cfgimg);
//cout << "Got to part 2" << endl;
// End from digites DataFiles.cpp

// FROM digites Readout.cpp

//        int i, ret = 0;
	int16_t ch;
	int16_t board;
//	uint16_t reclen;
	uint32_t reclen;
//	uint32_t wfmt;
//	uint64_t emtstamp = 0;
//	uint64_t lastemtstamp = 0;
	uint16_t info[2];
//	int naggr = 0;
	int nbr;
	reclen = info[1]; // Num of 32 bit words for the waveform (0 if waveforms are disabled)

    // Read data from binary file
  while (run) {              // read one devt_event record from binary file
  if (feof(InputDataFile)) { break; } // reached end of data file
  nbr = (int)fread(info, sizeof(uint16_t), 2, InputDataFile);  // read board/channel number and the num of samples (4 bytes)
  if (nbr != 2) break;
  board = (int)((info[0] >> 8) & 0xFF); // board
  ch = (int)(info[0] & 0xFF); // channel
  if ((ch < 0) || ((int)ch >= MAX_NCH) || (board < 0) || ((int)board >= MAX_NBRD)) {return -1;}
  if(RECORD_WAVEFORMS) {reclen = info[1];} // Num of 32 bit words for the waveform (0 if waveforms are disabled)

  if ((int)fread(&devt_event, sizeof(DEVT_BANK), 1, InputDataFile) < 1) { return -1; }
    det_event.channel = ch + board*16;
    det_event.timestamp = devt_event.TimeStamp*1000 + devt_event.FineTimeStamp; // psec
    //cout << det_event.timestamp << endl;
    det_event.finetimestamp = devt_event.FineTimeStamp; // in picosec
    det_event.lgate = devt_event.Energy;
    det_event.sgate = devt_event.Energy * devt_event.psd;
    det_event.psd = devt_event.psd;
    if(RECORD_WAVEFORMS) {memcpy(det_event.waveform, wf, sizeof(uint16_t) * NSAMP);}
    events_vec.push_back(det_event);
  }
    // time-sort all detector events 
  std::sort(events_vec.begin(), events_vec.end(), tsort);
    
  cout << "Total number of hits: " << events_vec.size() << endl;
  int event = 0;
  while (event < (int)events_vec.size()) {
	// initialize arrays and variables
//    for (int i = 0; i < 64; i++) {
//      channel[i] = -1;
//      psd[i] = -1;
//	    energy[i] = -1;
//	    if(RECORD_WAVEFORMS) {
//        for (int wave_sample = 0; wave_sample < NSAMP; wave_sample++) {
//          waveform[i][wave_sample] = 0;
//        }
//	    }
//    }

  // *****
  // Event building
  // *****
    mult = 0;
    starttime = events_vec[event].timestamp/1000.0;

    while (true) { // while events are within the event_duration window
      if (events_vec[event].timestamp - starttime > event_duration) {
        break;
      }
      channel[events_vec[event].channel] = events_vec[event].channel;
      energy[events_vec[event].channel] = events_vec[event].lgate;
//      psd[(int)det_evts[event].channel] = ((float)(det_evts[event].lgate) - (float)(det_evts[event].sgate)) / (float)det_evts[event].lgate;
      psd[events_vec[event].channel] = events_vec[event].psd;
      timestamp[events_vec[event].channel] = (double)(events_vec[event].timestamp/1000.0); // nsec
      finetimestamp[events_vec[event].channel] = events_vec[event].finetimestamp;
      if(RECORD_WAVEFORMS) {memcpy(waveform[events_vec[event].channel], events_vec[event].waveform, sizeof(uint16_t) * NSAMP);}
      mult++;
      event++;
    }
    t->Fill();

  } // while()
  fclose(InputDataFile);

  ofile->Write();  // write the ROOT File
  ofile->Close(); // close the output root file
  return 0;
}
