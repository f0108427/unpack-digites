ARCH          = ubuntu

CXX           =
ObjSuf        = o
SrcSuf        = cpp
ExeSuf        =
DllSuf        = so
OutPutOpt     = -o 

### NOTE, FOR THE NEXT 3 LINES TO WORK YOU HAVE TO 
### HAVE $(ROOTSYS)/bin in your path
ROOTCFLAGS   := $(shell /home/rogers/root/bin/root-config --cflags)
ROOTLIBS     := $(shell /home/rogers/root/bin/root-config --libs)
ROOTGLIBS    := $(shell /home/rogers/root/bin/root-config --glibs)

CXX           = g++
CXXFLAGS      = -O -Wall -fPIC
LD            = g++
LDFLAGS       = -O
SOFLAGS       = -shared

CXXFLAGS     += $(ROOTCFLAGS)
LIBS          = $(ROOTLIBS) $(SYSLIBS)
GLIBS         = $(ROOTGLIBS) $(SYSLIBS)

########################################################
# here is where you define the files that will be used 
# and the output file that will be created.
OBJS          = unpackDigites.o
PROGRAMS      = unpackDigites
#######################################################


.SUFFIXES: .$(SrcSuf) .$(ObjSuf) .$(DllSuf)


$(PROGRAMS):     $(OBJS)
		$(LD) $(LDFLAGS) $^ $(LIBS) $(OutPutOpt)$@
		@echo "$@ done"

clean:
		@rm -f $(OBJS) core
		@rm -f $(PROGRAMS)



.$(SrcSuf).$(ObjSuf):
	$(CXX) $(CXXFLAGS) -c $<
