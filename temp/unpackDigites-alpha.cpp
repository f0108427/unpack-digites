#include <iostream>
#include <fstream>
#include <math.h>
// root header files
#include </home/rogers/root/include/TFile.h>
#include </home/rogers/root/include/TTree.h>
#include </home/rogers/root/include/TTreeReader.h>
#include </home/rogers/root/include/TTreeIndex.h>
#include </home/rogers/root/include/TTreeReaderValue.h>
#include </home/rogers/root/include/TH1.h>
#include "unpackDigites.h"

#define EVENT_DURATION 100000 // 100 nsec

// To unpack waveforms the next two lines needed to be modified accordingly.  
#define NSAMP 56
#define RECORD_WAVEFORMS 0 // 1 to record waveforms, 0 for no waveforms
#define MAX_NCH 48
#define MAX_NBRD 3
#define PI 3.1415926
#define BD 13
#define AH 61

using namespace std;

//uint16_t event_duration = EVENT_DURATION/1000; // ns 
uint16_t event_duration = 20; // ns
double starttime, tmean[24], timestamp[48], tdiff[24], tdoffset[24];
//uint64_t tmean[24], timestamp[48];
uint16_t qhit[48], qshort[48], qmean[24];
uint16_t hitmult, barmult, bar[24], wf[NSAMP], waveform[48][NSAMP];
uint16_t finetimestamp[48];
uint16_t channel[48];
int tf, bf;
double position[24], posslope[24], diffspec[5][5];
float psd[48];
int main(int argc, char** argv)
{

    if (argc < 3) {
    // Tell the user how to run the program
      std::cerr << "Usage: " << argv[0] << " inputfile, outputfile, configfile (optional, default = det_config.txt)" << std::endl;
      return 0;
    }  

    SkyMap sm;
    char* inputfile = argv[1];
    char* outputfile = argv[2];
    TFile* ofile = new TFile(outputfile, "recreate");

    // Create the root tree and set up the branch structure
    TTree* t = new TTree("t", "Detector array tree");
    t->Branch("hitmult", &hitmult, "hitmult/s");
    t->Branch("barmult", &barmult, "barmult/s");
    t->Branch("channel[48]", &channel, "channel[48]/s");
    t->Branch("bar[24]", &bar, "bar[24]/s");
    t->Branch("time[48]", &timestamp, "timestamp[48]/D");
    t->Branch("finetime[48]", &finetimestamp, "finetimestamp[48]/s");
    t->Branch("tmean[24]", &tmean, "tmean[24]/D");
    t->Branch("tdiff[24]", &tdiff, "tdiff[24]/D");
    t->Branch("qhit[48]", &qhit, "qhit[48]/s");
    t->Branch("qmean[24]", &qmean, "qmean[24]/s");
    t->Branch("psd[48]", &psd, "psd[48]/F");
	//    if(RECORD_WAVEFORMS) {
	//	    char buf[100];
	//    	    sprintf(buf,"waveform[%i]/s",NSAMP);
	//	    t->Branch("waveform", &waveform, buf);}
    t->Branch("position[24]",&position,"position[24]/D");  
    t->Branch("diffspec[5][5]",&diffspec,"diffspec[5][5]/D");
    t->Branch("skymap",&sm,"ns/F:ew/F:ns_angle/F:ew_angle/F");
    t->Branch("topflag",&tf,"tf/I");
    t->Branch("botflag",&bf,"bf/I");

    ifstream config_file;
    if(argv[3]){
      config_file.open(argv[3]);
    }else{
      config_file.open("/home/analysis/Dropbox/software/unpackDigites/det_config.txt");
    }
    if(config_file.is_open()) {
	cout<<"Success opening DET configuration file"<<endl;
    } else {
	cout<<"Error opening DET configuration file"<<endl;
    }
    config_file >> tdoffset[0] >> tdoffset[1] >> tdoffset[2] >> tdoffset[3] >> tdoffset[4] >> tdoffset[5] >> tdoffset[6] >> tdoffset[7] >> tdoffset[16] >> tdoffset[17] >> tdoffset[18] >> tdoffset[19] >> tdoffset[20] >> tdoffset[21] >> tdoffset[22] >> tdoffset[23];
    config_file >> posslope[0] >> posslope[1] >> posslope[2] >> posslope[3] >> posslope[4] >> posslope[5] >> posslope[6] >> posslope[7] >> posslope[16] >> posslope[17] >> posslope[18] >> posslope[19] >> posslope[20] >> posslope[21] >> posslope[22] >> posslope[23];    
    cout<<"tdiff offsets: "<<tdoffset[1]<<" "<<tdoffset[2]<<" "<<tdoffset[3]<<"..."<<endl;
    cout<<"position slopes:: "<<posslope[1]<<" "<<posslope[2]<<" "<<posslope[3]<<"..."<<endl;
    config_file.close();

    // Unpack the digites binary file
    DEVT_BANK devt_event;
    DET_BANK det_event;
    std::vector<DET_BANK> det_evts;
    FILE *InputDataFile = NULL;
    //    char InputDataFileName[500] = "";			// Input data file (off-line run) 

    int run = 1;
    
    // From digites DataFiles.cpp, to read header information from the binary file
    //
    // Open binary file and read header information, write to file

    //	char fname[200];
    char DataFormat;
    char *cfgimg;
    uint32_t header[128];
    uint32_t nbcfg;
    FILE *cfg;
//	uint32_t Nsamples;
//	uint32_t DigitizerModel, DppType, NumBrd, NumPhyCh, Tsampl, Nbit;
//	int16_t WaveformEnabled;

//	strcpy(InputDataFileName, "Run0.dat");
//        InputDataFile = fopen(InputDataFileName, "rb");
	InputDataFile = fopen(inputfile, "rb");
	if (InputDataFile == NULL) {
//		msg_printf(MsgLog, "ERROR: Can't open Input Data File %s\n", fname);
		return -1;
	}
cout << "Input file opened" << endl;
	// read the header of the data file
	if (fread(&DataFormat, sizeof(char), 1, InputDataFile) < 1) { // Data format (version)
//		msg_printf(MsgLog, "ERROR: Invalid format for a raw data file\n");
		return -1;
	}
	cout << "DataFormat = " << DataFormat << endl;
	if (DataFormat == 0) {
		fread(header, sizeof(uint32_t), 1, InputDataFile); // header[0] = number of 32-bit words in header
		fread(header+1, sizeof(uint32_t), header[0]-1, InputDataFile); // read the rest of the header based on header[0]-1 remaining 32-bit words
		cout << "Num 32-bit words in header: " << header[0] << endl;
//		Nsamples       = header[1]; 
//		DigitizerModel = header[2];
//		DppType        = header[3];
//		NumBrd         = header[4];
//		NumPhyCh       = header[5];
//		Tsampl         = header[6];
//		Nbit           = header[7];
	}
//	if (Nsamples == 0) { WaveformEnabled = 0;} else { WaveformEnabled = 1;}
//	if(RECORD_WAVEFORMS) {cfgimg = (char *)malloc(1024*1024);}
	cfgimg = (char *)malloc(1024*1024);
  	char* log = ".log";
	char cfg_file[strlen(outputfile)+strlen(log)+1];
	snprintf(cfg_file, sizeof( cfg_file ), "%s%s", outputfile, log );
	cfg = fopen(cfg_file, "w");
	if (cfg != NULL) {
		fread(&nbcfg, sizeof(uint32_t), 1, InputDataFile);
		if (fread(cfgimg, sizeof(char), nbcfg, InputDataFile) < nbcfg) {
			free(cfgimg);
//			msg_printf(MsgLog, "ERROR: Invalid data in input file\n");
			return -1;
		}
		fwrite(cfgimg, sizeof(char), nbcfg, cfg);
	}
	fclose(cfg);
	free(cfgimg);
//cout << "Got to part 2" << endl;
// End from digites DataFiles.cpp

// FROM digites Readout.cpp

//        int i, ret = 0;
	int16_t ch;
	int16_t board;
//	uint16_t reclen;
	uint32_t reclen;
//	uint32_t wfmt;
//	uint64_t emtstamp = 0;
//	uint64_t lastemtstamp = 0;
	uint16_t info[2];
//	int naggr = 0;
	int nbr;

//	*nb=0;

//	while (1) {
//		Waveform_t *Wfm;
//		uint64_t ts;

//		if (feof(InputDataFile)) { break; } // reached end of data file
//		// Read board and channel number
//		nbr = (int)fread(info, sizeof(uint16_t), 2, InputDataFile);  // read board/channel number and the num of samples (4 bytes)
//		if (nbr != 2) break;
//		b = (int)((info[0] >> 8) && 0xFF);
//		ch = (int)(info[0] & 0xFF);
		reclen = info[1]; // Num of 32 bit words for the waveform (0 if waveforms are disabled)
//		if ((ch < 0) || ((int)ch >= MAX_NCH) || (b < 0) || ((int)b >= MAX_NBRD))
//			return -1;
//		// Read one event from the file
//		if ((int)fread(&devt_evnt, sizeof(DEVT_BANK), 1, InputDataFile) < 1)			return -1;
//		devt_evnt.TimeStamp += emtstamp;
//		if (lastemtstamp < devt_evnt.TimeStamp)
//			lastemtstamp = devt_evnt.TimeStamp;

		// read the waveform and format and write them to the buffer 
//		if ((reclen > 0) && WDcfg.WaveformEnabled)  { 
//			AllocateWaveform(&Wfm, reclen);
//			Wfm->Ns = reclen;
//			if ((int)fread(&wfmt, sizeof(uint32_t), 1, InputDataFile) < 1)
//				return -1;
//			Wfm->DualTrace = (wfmt >> 31) & 0x01;
//			for(i=0; i<MAX_NTRACES; i++) {
//				Wfm->TraceSet[i] = (wfmt >> (i*4)) & 0xF;
//				if (Wfm->TraceSet[i] == 0xF)
//					Wfm->TraceSet[i] = -1;
//			}
//			if ((int)fread(Wfm->AnalogTrace[0], sizeof(uint32_t), reclen/2, InputDataFile) < (reclen/2)) 
//				return -1
//			if (Wfm->DualTrace) {
//				if ((int)fread(Wfm->AnalogTrace[1], sizeof(uint32_t), reclen/2, InputDataFile) < (reclen/2)) 
//					return -1;
//			}
//			evnt.Waveforms = Wfm;
//		} else {
//			evnt.Waveforms = NULL;
//		}
//		*nb += 2 * sizeof(uint16_t) + sizeof(GenericDPPEvent_t);
//		r = PushEvent(b, ch, &evnt);
//		naggr++;
//		if ((r == 0) || (naggr == 64))
//			break;
//	}
//	Stats.RxByte_cnt += *nb;

// End from digites Readout.cpp
    
// ***** READ ALL DATA FROM BINARY FILE ***** //
    while (run) {              // read one devt_event record from binary file
	if (feof(InputDataFile)) { break; } // reached end of data file
	nbr = (int)fread(info, sizeof(uint16_t), 2, InputDataFile);  // read board/channel number and the num of samples (4 bytes)
	if (nbr != 2) break;
	board = (int)((info[0] >> 8) & 0xFF); // board
	ch = (int)(info[0] & 0xFF); // channel
	if ((ch < 0) || ((int)ch >= MAX_NCH) || (board < 0) || ((int)board >= MAX_NBRD)) {return -1;}
	if(RECORD_WAVEFORMS) {reclen = info[1];} // Num of 32 bit words for the waveform (0 if waveforms are disabled)

        if ((int)fread(&devt_event, sizeof(DEVT_BANK), 1, InputDataFile) < 1) { return -1; }
        det_event.channel = ch + board*16;
        //	cout << "det_event.channel = " << det_event.channel << endl;
	det_event.timestamp = (double)devt_event.TimeStamp + ((double)devt_event.FineTimeStamp)/1000.0; // nsec
        //	cout << det_event.timestamp << endl;
	det_event.finetimestamp = devt_event.FineTimeStamp; // 0 to 2000 for 0 to 2 ns
	det_event.lgate = devt_event.Energy;
	det_event.sgate = devt_event.Energy * (1-devt_event.psd);
	det_event.psd = devt_event.psd; // (long-short)/long integrals
        if(RECORD_WAVEFORMS) {memcpy(det_event.waveform, wf, sizeof(uint16_t) * NSAMP);}

        // Add hit to the det_evts vector
        det_evts.push_back(det_event);
    }
    //    cout << "Just finished run loop" << endl;

// ***** TIME SORT ALL DATA *****
    // Time sort all of the events collected from the raw binary file
    std::sort(det_evts.begin(), det_evts.end(), tsort);
// ***** //

    cout << "Total number of hits: " << det_evts.size() << endl;
    int event = 0;

//    while (event < 100000) {
    while (event < (int)det_evts.size()-10) { // For later:  Crashes when -10 is removed, which should work
	// initialize arrays and variables
        for (int pmt_index = 0; pmt_index < 48; pmt_index++) {
            channel[pmt_index] = -1;
            psd[pmt_index] = -1;
	    qhit[pmt_index] = -1;
	    if(RECORD_WAVEFORMS) {
              for (int wave_sample = 0; wave_sample < NSAMP; wave_sample++) {
                 waveform[pmt_index][wave_sample] = 0;
              }
	    }
        }
        for (int bar_index = 0; bar_index <24; bar_index++) { 
	    bar[bar_index] = -1; 
	    tdiff[bar_index] = -1; 
	    qmean[bar_index] = -1; 
	    position[bar_index] = -1; 
	    tmean[bar_index] = -1; 
	}
        hitmult = 0;
        barmult = 0;
        starttime = det_evts[event].timestamp;

// ***** EVENT CONSTRUCTION *****
        while (true) { // while events are within the event_duration window
            //cout << det_evts[event].timestamp - starttime << endl; 
            if (det_evts[event].timestamp - starttime > event_duration) {
                break;
            }
            channel[det_evts[event].channel] = det_evts[event].channel;
            qhit[det_evts[event].channel] = det_evts[event].lgate;
//            psd[(int)det_evts[event].channel] = ((float)(det_evts[event].lgate) - (float)(det_evts[event].sgate)) / (float)det_evts[event].lgate;
            psd[det_evts[event].channel] = det_evts[event].psd;
            timestamp[det_evts[event].channel] = det_evts[event].timestamp;
            finetimestamp[det_evts[event].channel] = det_evts[event].finetimestamp;
            if(RECORD_WAVEFORMS) {memcpy(waveform[det_evts[event].channel], det_evts[event].waveform, sizeof(uint16_t) * NSAMP);}
            hitmult++;
            event++;
        } // while (true) event construction
//	cout << "Just finished the event-building loop" << endl;

        if (hitmult > 1) {
            // calculate bar positions and tmean values and set the bar hit-vector 
//	    ***** The followiing is for when the ends of the detectors are in neighboring digitizer channels
/*            for (int bar_index = 0; bar_index < 24; bar_index++) {}
                if ((channel[2*bar_index + 1] == 2*bar_index + 1) && (channel[2*bar_index] == 2*bar_index)) {} // if two pmt's on the same bar fired
                    tdiff[bar_index] = (float)((timestamp[2*bar_index + 1]/1000.0 - timestamp[2*bar_index]/1000.0) + tdoffset[bar_index]); // ns 
                    qmean[bar_index] = (uint16_t)sqrt(qhit[2*bar_index + 1] * qhit[2*bar_index]);
                    tmean[bar_index] = (timestamp[2*bar_index + 1] + timestamp[2*bar_index]) / 2;
                    position[bar_index]=tdiff[bar_index]*posslope[bar_index];
//                    position[bar_index+1]=tdiff[bar_index+1]*posslope[bar_index+1]; // + posintercept[bar_index+1];
                    bar[bar_index] = bar_index;
//		    datafile << event << " "<< bar[bar_index + 1] <<" "<< tdiff[bar_index + 1] << " " << qmean[bar_index + 1] << endl;
*/	    
//	    ***** The following is for when the ends of the detectors are in cards 0 and 1 with same channels
//            for (int bar_index = 0; bar_index < 16; bar_index++) {}
//                if ((channel[bar_index + 16] == bar_index + 16) && (channel[bar_index] == bar_index)) {} // if two pmt's on the same bar fired
//                    tdiff[bar_index + 1] = (int64_t)(timestamp[bar_index + 16] - timestamp[bar_index]); // + tdoffset[bar_index+1];
//                    qmean[bar_index + 1] = (uint16_t)sqrt(qhit[bar_index + 16] * qhit[bar_index]);
//                    tmean[bar_index + 1] = (timestamp[bar_index + 16] + timestamp[bar_index]) / 2;
//                    position[bar_index + 1]=tdiff[bar_index + 1]*posslope[bar_index + 1];
//                    position[bar_index+1]=tdiff[bar_index+1]*posslope[bar_index+1] + posintercept[bar_index+1];
//                   bar[bar_index + 1] = bar_index + 1;
//		    datafile << event << " "<< bar[bar_index + 1] <<" "<< tdiff[bar_index + 1] << " "
//			<< qmean[bar_index + 1] << endl;

//	    ***** The following is for the eight CMDA bars with PMT's in channels n and n+8
            for (int bar_index = 0; bar_index < 8; bar_index++) {
                if ((channel[bar_index + 8] == bar_index + 8) && (channel[bar_index] == bar_index)) { // if two pmt's on the same bar fired
                    tdiff[bar_index + 1] = timestamp[bar_index + 8] - timestamp[bar_index] + tdoffset[bar_index]; //
                    qmean[bar_index + 1] = sqrt(qhit[bar_index + 8] * qhit[bar_index]);
                    tmean[bar_index + 1] = (timestamp[bar_index + 8] + timestamp[bar_index]) / 2;
                    position[bar_index + 1]=tdiff[bar_index + 1]*posslope[bar_index];
                    bar[bar_index + 1] = bar_index + 1;
//		    datafile << event << " "<< bar[bar_index + 1] <<" "<< tdiff[bar_index + 1] << " " << qmean[bar_index + 1] << endl;
		    
                    barmult++;
                }
            }
     	    if(barmult==2) { // this assumes 1 is top and 1 is bot.  We will refine this soon to include higher multiplicity events
            	for(int top_bar=1;top_bar<5;top_bar++) { 
                    if(bar[top_bar]==top_bar) {
		    tf = top_bar;
		    }
		}
            	for(int bot_bar=1;bot_bar<5;bot_bar++) {
	   	    if(bar[bot_bar+4]==bot_bar+4) {
		    bf = bot_bar;
                    }
	        }
	    // populate the differential spectra and flag the upper and lower detectors
		if(tf>-1 && bf>-1) {
                    diffspec[tf][bf] = position[tf] - position[bf+4];
		    sm.ns = tf-bf;
                    sm.ew = diffspec[tf][bf];
		}
                if(abs(sm.ns)<1000) {sm.ns_angle = (180/PI)*atan(BD*sm.ns/AH);} // build a map of ns & ew angles in order to fill each cell of the angle map by
                if(abs(sm.ew)<1000) {sm.ew_angle = (180/PI)*atan(sm.ew/AH);} // multiplying the ew by cell width, ns by horizontal bar separation/disparity
            }
	} // if (hitmult > 1)
	t->Fill();
//	cout << "Just wrote to tree" << endl;
//        if (barmult > 0) { t->Fill(); }
    } // while() for going through all events in det_event vector (i.e. all events in file)
    cout << "Finished writing tree\n" << endl;
    fclose(InputDataFile);

    ofile->Write();  // write the ROOT File
    ofile->Close(); // close the output root file
//    datafile.close();
    return 0;
}
