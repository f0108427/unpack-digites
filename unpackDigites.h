//****************************************************************************
// Waveform Data Structure
//****************************************************************************
typedef struct
{ 
    int32_t Ns;                                         // Num of samples
    uint16_t *AnalogTrace[2];           // Analog traces (samples); the 2nd trace is enabled if DualTrace=1
    uint8_t  *DigitalTraces;            // Digital traces (1 bit = 1 trace)
        int8_t DualTrace;                               // Dual Analog Traces
        int8_t TraceSet[6];   // Traces Setting
} Waveform_t;

//****************************************************************************
// Generic Event Data Structure 
//****************************************************************************
typedef struct
{
        uint64_t TimeStamp;                     // 64 bit coarse time stamp (in ns)
        uint16_t FineTimeStamp;         // Fine time stamp (interpolation) in ps
        uint16_t Energy;                        // Energy (charge for DPP_PSD/CI or pulse height for DPP_PHA)
        float psd;                                      // Pulse Shape Discrimination (PSD = (Qlong-Qshort)/Qlong). Only for DPP_PSD. Range 0 to 1023.
        Waveform_t *Waveforms;          // Pointer to waveform data (NULL if the waveform mode is not enabled)
        uint16_t Flags;                         // Event Flags (e.g. pile-up, saturation, etc...)
} DEVT_BANK;

typedef struct {
    uint16_t channel;    // channel number
    uint64_t timestamp; 
    uint16_t finetimestamp; // interpolated time
    uint16_t lgate;      // long gate
    uint16_t sgate;      // short gate
    float psd;
    uint16_t waveform[48]; //waveform
} DET_BANK;

inline bool tsort(DET_BANK i,DET_BANK j) {return (i.timestamp<j.timestamp); }
typedef struct {
   float ns, ew;
   float ns_angle, ew_angle;
} SkyMap;

